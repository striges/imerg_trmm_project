# coding=utf-8
# Multiple core processing
import asyncio
import os
import sys
from argparse import ArgumentError
from pprint import pprint
import pandas as pd

import add_advanced_geometry
import add_basic_geometry
import contour_polygon_filling
import save_to_db
import utils
import array_contour

import arcpy


async def get_file(t):
    if utils.mode.lower() == "imerg":
        from retrieve_remote_file import RemoteImergFile
        r = RemoteImergFile(pd.Timestamp(t))
    elif utils.mode.lower() == "trmm":
        from retrieve_remote_file import RemoteTrmmFile
        r = RemoteTrmmFile(pd.Timestamp(t))
    else:
        raise ValueError("Not supported mode")
    await r.download()
    return r.as_arcpy_raster()


async def start_mp(row=None):
    arcpy.CheckOutExtension("Spatial")
    arcpy.env.workspace = utils.temp_folder
    arcpy.env.overwriteOutput = True

    # 1. We need setup folders
    utils.create_dirs([utils.cnt_folder, utils.cnt_polygon_folder, utils.stage1_folder, utils.stage2_folder], False)

    # 1. Download File
    if isinstance(row, dict):
        raster = await get_file(row["ISO_TIME"])
        os.environ["ISO_TIME"] = row["ISO_TIME"]
        os.environ["EYE_LAT"] = str(row["LAT"])
        os.environ["EYE_LON"] = str(row["LON"])
        os.environ["STORM_DR"] = str(row["STORM_DR"])
        os.environ["NAME"] = str(row["NAME"])
    elif isinstance(row, str):
        raster = await get_file(row)
        os.environ["ISO_TIME"] = row
        assert "EYE_LAT" in os.environ
        assert "EYE_LON" in os.environ
        assert "STORM_DR" in os.environ
        assert "NAME" in os.environ
    else:
        raise ArgumentError("Wrong calling argument")

    # 2. Get contour at each level
    cnt_output_path = os.path.join(utils.base_folder, utils.cnt_folder)
    contour_output_files = array_contour.execute(raster, utils.contour_level, cnt_output_path)
    pprint(contour_output_files)

    # 3. Fill contour, we need refresh filelist in cnt_folder
    cnt_input = contour_output_files
    cnt_output = []
    for p in cnt_input:
        cnt_output_polygon_path = \
            utils.relocate(p, os.path.join(utils.base_folder, utils.cnt_polygon_folder), ".shp").replace("-", "_")
        if contour_polygon_filling.execute(p, cnt_output_polygon_path, raster):
            cnt_output.append(cnt_output_polygon_path)

    # 4. Calculate metric for each polygon in each file
    bsm_input = cnt_output
    bsm_output = []
    for p in bsm_input:
        bsm_output_path = \
            utils.relocate(p, os.path.join(utils.base_folder, utils.stage1_folder), ".shp").replace("-", "_")
        add_basic_geometry.execute(p, bsm_output_path)
        bsm_output.append(bsm_output_path)

    # 5. Calculate summary metric in each file.
    asm_input = bsm_output
    for p in asm_input:
        asm_output_path = \
            utils.relocate(p, os.path.join(utils.base_folder, utils.stage2_folder), ".shp").replace("-", "_")
        # Nothing to collect, results will be inserted final storage
        line = add_advanced_geometry.execute(p, asm_output_path)
        save_to_db.insert_line(os.environ["ISO_TIME"], os.environ["NAME"],
                               contour_polygon_filling.retrieve_rain_level(p), line[1])

# Main
if __name__ == "__main__":
    _loop = asyncio.get_event_loop()
    _loop.run_until_complete(start_mp(sys.argv[1]))
    _loop.close()
