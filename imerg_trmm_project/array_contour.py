﻿__author__ = 'jtang8756'

import os
import sys
import utils
from utils import create_dirs, relocate

import arcpy


def execute(input_raster, contour_levels, output_dir=None, mask=None):
    output_dir = output_dir or os.path.join(utils.base_folder, utils.cnt_folder)
    arcpy.env.overwriteOutput = True
    d = input_raster
    # Apply mask if provided.
    d1 = arcpy.sa.ExtractByMask(d, mask) if mask is not None else d
    d2 = arcpy.sa.Con(arcpy.sa.IsNull(d1), 0, d1)
    # d21.save(r"E:\scratch\T1.img")
    output_feat_list = []
    for i in contour_levels:
        output_feat = relocate(input_raster.catalogPath, output_dir, "shp", f"_{i}mm")
        print(f"Contour prate={i}mm to {output_feat}")
        arcpy.sa.ContourList(d2, output_feat, [i])
        result = arcpy.GetCount_management(output_feat)
        count = int(result.getOutput(0))
        if count:
            output_feat_list.append(output_feat)
    return output_feat_list

