import sqlite3
import os
import sys


def insert_line(time, name, level, line):
    conn.execute(f"INSERT OR REPLACE INTO Result (ISO_TIME, NAME, LEVEL, PAYLOAD) VALUES(?, ?, ?, ?);",
                 (time, name, level, "".join(line)))
    conn.commit()
    print(f"{name}@{time}: Saved")


def dump_csv():
    if __name__ != "__main__":
        print("YOU SHALL NOT CALL THIS FROM OTHER FUNCTION")
        return
    level = float(sys.argv[2])
    cur = conn.cursor()
    cur.execute("SELECT * FROM Result WHERE LEVEL=?", (level,))
    with open(f'{os.environ["CASENAME"]}_{level:.1f}mm.csv', "w") as g:
        header = ",".join(
            ['Timestamp', 'rain_rate', 'TC', 'dispersiveness,total_area,total_convex_area,count', 'closure', 'frag',
             'asymmetry/roundness',
             'extent_move_NE,extent_move_NW,extent_move_SW,extent_move_SE',
             'extent_math_FR,extent_math_FL,extent_math_BL,extent_math_BR',
             'L_AreaRatio', 'L_AREA', 'L_PERIM', 'L_AREA_CVX', 'L_PERIM_CVX', 'L_SOLIDITY', 'L_CONVEXITY', 'L_COMPACT'])
        g.write(f"{header}\n")
        for row in cur:
            g.write(",".join(map(str, row)))
            g.write("\n")
    pass


def create_db():
    conn.execute('''CREATE TABLE IF NOT EXISTS "Result" (
      "ISO_TIME" TEXT NOT NULL,
      "LEVEL" INTEGER NOT NULL,
      "NAME" TEXT,
      "PAYLOAD" TEXT
    );''')
    conn.execute('''
    CREATE UNIQUE INDEX IF NOT EXISTS "AAA"
    ON "Result" (
      "ISO_TIME" ASC,
      "LEVEL" ASC,
      "NAME" ASC
    );''')


if __name__ == "__main__":
    conn = sqlite3.connect(f"{sys.argv[1]}.db")
    os.environ["CASENAME"] = sys.argv[1]
    create_db()
    dump_csv()
else:
    import utils
    conn = sqlite3.connect(f"{utils.mode}.db")
    create_db()
