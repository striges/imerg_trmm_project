import os
import sys
from unittest import TestCase

import aiohttp
import pandas as pd
from aiounittest import AsyncTestCase

import utils


class RemoteFile:

    def __init__(self, target_time: pd.Timestamp):
        self.target_time = target_time

    async def download(self):
        if os.path.isfile(self.full_path):
            return
        async with aiohttp.ClientSession(cookie_jar=aiohttp.CookieJar()) as session:
            async with session.get(self.url) as r:
                print(f"Downloading from {self.url}")
                url1 = r.url
            async with session.get(url1,
                                   auth=aiohttp.BasicAuth(os.environ["E_USR"], os.environ["E_PWD"])) as r:
                assert r.status == 200
                with open(self.full_path, "wb") as g:
                    g.write(await r.content.read())

    @property
    def full_path(self):
        return os.path.join(utils.base_folder, self.filename)


class RemoteImergFile(RemoteFile):

    def __init__(self, target_time: pd.Timestamp):
        super().__init__(target_time)

    def as_arcpy_raster(self):
        import arcpy
        arcpy.env.compression = "LZ77"
        arcpy.env.overwriteOutput = True
        import h5py
        import numpy
        ds = h5py.File(os.path.join(utils.base_folder, self.filename), 'r')
        frame = numpy.squeeze(ds['/Grid/precipitationCal'][:])
        frame = numpy.flipud(frame.T)
        ras = arcpy.NumPyArrayToRaster(frame, arcpy.Point(-180.0, -90.0), 0.1, 0.1, -9999.9)
        sr = arcpy.SpatialReference(4326)
        arcpy.DefineProjection_management(ras, sr)
        out_name = self.full_path.replace(".HDF5", ".img").replace("3B-HHR.MS.MRG.3", "").replace(".0360.V06B", "")
        arcpy.CopyRaster_management(ras, out_name)
        print(f"Saving raster to {out_name}")
        return arcpy.Raster(out_name)  # If we want a temporary file, we use that file

    @property
    def filename(self):
        """3B-HHR.MS.MRG.3IMERG.20150101-S003000-E005959.0030.V06B.HDF5"""
        _t1 = self.target_time.strftime("%Y%m%d-S%H%M%S")
        _t2 = (self.target_time + pd.Timedelta(minutes=29, seconds=59)).strftime("E%H%M%S")
        _t3 = (self.target_time.hour * 60 + self.target_time.minute)
        return f"3B-HHR.MS.MRG.3IMERG.{_t1}-{_t2}.{_t3:04d}.V06B.HDF5"

    @property
    def url(self):
        _y = self.target_time.year
        _d = self.target_time.dayofyear
        return f"https://gpm1.gesdisc.eosdis.nasa.gov/data/GPM_L3/GPM_3IMERGHH.06/{_y}/{_d}/{self.filename}"


class RemoteTrmmFile(RemoteFile):

    def __init__(self, target_time: pd.Timestamp):
        super().__init__(target_time)

    def as_arcpy_raster(self):
        import arcpy
        arcpy.env.compression = "LZ77"
        arcpy.env.overwriteOutput = True
        from pyhdf.SD import SD, SDC
        import numpy
        ds = SD(os.path.join(utils.base_folder, self.filename), SDC.READ)
        frame = numpy.squeeze(ds.select('precipitation')[:])
        frame = numpy.flipud(frame.T)
        ras = arcpy.NumPyArrayToRaster(frame, arcpy.Point(-180.0, -50.0), 0.25, 0.25)
        sr = arcpy.SpatialReference(4326)
        arcpy.DefineProjection_management(ras, sr)
        out_name = self.full_path.replace(".7.HDF", ".img").replace(".7A.HDF", ".img").replace("3B42", "TRMM_3B42")
        arcpy.CopyRaster_management(ras, out_name)
        print(f"Saving raster to {out_name}")
        return arcpy.Raster(out_name)  # If we want a temporary file, we use that file

    @property
    def filename(self):
        """3B42.20050111.03.7A.HDF"""
        _t1 = self.target_time.strftime("%Y%m%d.%H")
        _t2 = "7A" if self.target_time < pd.Timestamp("2010-10-01 00:00:00") else "7"
        return f"3B42.{_t1}.{_t2}.HDF"

    @property
    def url(self):
        # NOTE: TRMM put next-day 00Z file in the current day folder, so we need shift it back.
        _y = (self.target_time - pd.Timedelta(hours=1)).year
        _d = (self.target_time - pd.Timedelta(hours=1)).dayofyear
        return f"https://disc2.gesdisc.eosdis.nasa.gov/data/TRMM_L3/TRMM_3B42.7/{_y}/{_d}/{self.filename}"


class TestRemoteFile(AsyncTestCase):

    async def test_download_imerg_file(self):
        r = RemoteImergFile(pd.Timestamp("2014-10-26 09:00:00"))
        await r.download()
        out = os.path.join(utils.temp_folder, r.filename)
        self.assertTrue(os.path.exists(out))
        os.unlink(out)

    async def test_save_imerg_raster(self):
        r = RemoteImergFile(pd.Timestamp("2014-10-26 09:00:00"))
        await r.download()
        r.as_arcpy_raster()

    async def test_download_trmm_file(self):
        r = RemoteTrmmFile(pd.Timestamp("2014-10-26 09:00:00"))
        await r.download()
        out = os.path.join(utils.temp_folder, r.filename)
        self.assertTrue(os.path.exists(out))
        os.unlink(out)

    async def test_save_trmm_raster(self):
        r = RemoteTrmmFile(pd.Timestamp("2014-10-26 09:00:00"))
        await r.download()
        r.as_arcpy_raster()
