# coding=utf-8

import os
import datetime
import time
import pickle
import math
import sys
import logging

import pyproj
import utils
from add_basic_geometry import add_basic_geometry_attr
from create_radiant_line import RadiantLine

import numpy

import arcpy
import arcpy.da


from contour_polygon_filling import distance_to_eye


def generate_all_polygon_metric(polygon):

    eye_lon = float(os.environ["EYE_LON"])
    eye_lat = float(os.environ["EYE_LAT"])
    move_dir = float(os.environ["STORM_DR"])

    with utils.TempArcpyEnv():

        # list for dispersiveness
        l_dispersive = []
        # list for fragmentation
        l_frag = []
        # list for roundness
        l_round = []
        # list to collect areas
        _areas_list = []

        out0 = utils.relocate(polygon, suffix="_all_cvx")
        arcpy.MinimumBoundingGeometry_management(polygon, out0, "CONVEX_HULL", "ALL", mbg_fields_option="MBG_FIELDS")
        add_basic_geometry_attr(out0)
        with arcpy.da.SearchCursor(out0, ["AREA"]) as cur:
            row = next(cur)
            total_cvx_areas = row[0]

        with arcpy.da.SearchCursor(polygon, ["AREA", "AREA_CVX", "PERIM", "PERIM_CVX", "SHAPE@XY"]) as cur:
            cur.reset()
            for row in cur:
                l_dispersive.append([row[0], distance_to_eye(*row[-1])])
                # Fragment
                l_frag.append((row[0], row[1]))
                # Roundness
                l_round.append((row[0], row[2]))

        # Dispersiveness = sum(area / total_area * dist_weight)
        areas = numpy.array(l_dispersive)
        if areas.shape != (0,):
            total_areas = numpy.sum(areas[:, 0])
            area_frac = areas[:, 0] / total_areas
            dist_weight = areas[:, 1] / utils.search_radius
            # By the way, we calculate total area and total part count
            count = areas.shape[0]
            dispersiveness = "%f,%f,%f,%d," % (
                numpy.sum(area_frac * dist_weight),
                total_areas, total_cvx_areas,
                count)
            fragmentation = "%f," % ((total_areas / total_cvx_areas) / (math.log(count) + 1))
        else:
            dispersiveness = ",,0,"
            fragmentation = ","

        # Asymmetry/Roundness
        rareas = numpy.array(l_round)
        if rareas.shape != (0,):
            max_rareas = rareas[numpy.argmax(rareas, 0)[0]]
            # R = base_roundness * size_factor
            __S1 = numpy.log(max_rareas[0]) / numpy.log(total_areas)
            __R = (4 * max_rareas[0] * math.pi / numpy.square(max_rareas[1]) * __S1)
            roundness = "%f," % (1 - __R)
        else:
            roundness = ","

        # Now we we can do closure in old way
        with RadiantLine(lon=eye_lon, lat=eye_lat, r_start=0, r_end=utils.search_radius / 1e3) as radiant:
            out1 = utils.relocate(polygon, suffix="_cl")
            arcpy.Intersect_analysis([polygon, radiant.temp_name], out1, join_attributes="ALL", output_type="INPUT")
            with arcpy.da.SearchCursor(out1, ["SHAPE@", "DEG"]) as q:
                count = set()
                for k in q:
                    count.add(k[1])
                closure_str = "%.2f" % (len(count) / 360.0)
        closure_str += ","

        extent_mv = {"1": [0] * 90, "2": [0] * 90, "3": [0] * 90, "4": [0] * 90}
        extent_nat = {"1": [0] * 90, "2": [0] * 90, "3": [0] * 90, "4": [0] * 90}
        with RadiantLine(lon=eye_lon, lat=eye_lat, r_start=0, r_end=600, direction=int(move_dir)) as radiant:
            out2 = utils.relocate(polygon, suffix="_ex")
            arcpy.Intersect_analysis(in_features=[polygon, radiant.temp_name],
                                     out_feature_class=out2, join_attributes="ALL",
                                     output_type="INPUT")
            with arcpy.da.SearchCursor(out2, ["SHAPE@", "QUAD", "MOVE", "DEG", "MV_DEG"]) as q:
                for k in q:
                    quad = k[1]
                    move = k[2]
                    geom = k[0]
                    deg = int(k[3]) % 90
                    mv_deg = int(k[4]) % 90
                    for part in geom.getPart():
                        for pt in part:
                            dist = distance_to_eye(pt.X, pt.Y) / 1000.0
                            extent_mv[move][mv_deg] = max(extent_mv[move][mv_deg], dist)
                            extent_nat[quad][deg] = max(extent_nat[quad][deg], dist)
            extent_mv_str = "%.2f,%.2f,%.2f,%.2f" % tuple(max(extent_mv[q]) for q in ("1", "2", "3", "4"))
            extent_str = "%.2f,%.2f,%.2f,%.2f" % tuple(max(extent_nat[q]) for q in ("1", "2", "3", "4"))
        extent_mv_str += ","
        extent_str += ","

    # Get largest polygons and copy their attributes
    max_area = 0
    all_fields = ["AREA", "PERIM", "AREA_CVX", "PERIM_CVX", "SOLIDITY", "CONVEXITY", "COMPACT"]
    with arcpy.da.SearchCursor(polygon, all_fields) as cur:
        for row in cur:
            if row[0] > max_area:
                max_area = row[0]
                attr = list(map(str, row))  # row is a tuple
    large_attr_list = ["L_AreaRatio"] + [f"L_{p}" for p in all_fields]
    # We need again process large_attr_list to convert to csv strings
    largest_str = [str(max_area / total_areas)] + attr

    # let us return field name first, then field values
    fields = ["dispersiveness,total_area,total_convex_area,count", "closure", "frag", "asymmetry/roundness",
              "extent_move_NE,extent_move_NW,extent_move_SW,extent_move_SE",
              "extent_math_FR,extent_math_FL,extent_math_BL,extent_math_BR"] + large_attr_list
    values = [dispersiveness, closure_str, fragmentation, roundness,
              extent_mv_str,
              extent_str] + [",".join(largest_str)]

    assert(len(",".join(fields).split(",")) == len("".join(values).split(",")))
    return fields, values


def clean_fields(polygon):
    for field in arcpy.ListFields(polygon):
        if field.name.find("_1") != -1 or field.name.endswith("_"):
            arcpy.DeleteField_management(polygon, field.name)
            print("%s: Field %s deleted" % (polygon, field.name))
    pass


def execute(input_feat, output_feat):
    # Copy to destination
    arcpy.CopyFeatures_management(input_feat, output_feat)
    print("Copying %s -> %s" % (input_feat, output_feat))
    # Get timestamp
    q = os.path.basename(input_feat)
    # Clean fields
    clean_fields(output_feat)
    # Dispersiveness and EVERYTHING
    dispersive = generate_all_polygon_metric(output_feat)
    print("OK", dispersive)
    return dispersive

