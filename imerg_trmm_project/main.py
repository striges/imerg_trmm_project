import asyncio
import os
import subprocess
import sys
from concurrent.futures.thread import ThreadPoolExecutor

import arcpy
import pandas as pd
import utils

print(arcpy.CheckOutExtension("Spatial"))
arcpy.env.overwriteOutput = True
arcpy.env.workspace = utils.temp_folder

df = pd.read_csv("ibtracs.csv", header=[0], index_col=[0])


def get_case(row_num):
    t, y, x, d, n = df.loc[row_num, ['ISO_TIME', 'LAT', 'LON', 'STORM_DR', 'USA_ATCFID']]
    print(f"Case time: {t}@({x}, {y})->{d}")
    return {"ISO_TIME": t, "LON": x, "LAT": y, "STORM_DR": d, "NAME": n}


async def run_radar(loop):
    def stub(row_num):
        row = get_case(row_num)
        cmd = [sys.executable, "mp_start.py", row['ISO_TIME'], utils.mode]
        print(f"Calling: {' '.join(cmd)}")
        p = subprocess.Popen(cmd, env=os.environ.update({
            "CASENAME": sys.argv[1],
            "EYE_LON": str(row["LON"]),
            "EYE_LAT": str(row["LAT"]),
            "STORM_DR": str(row["STORM_DR"]),
            "NAME": str(row["NAME"])
        }))
        p.wait()

    # noinspection PyUnreachableCode
    if "BATCH" not in os.environ:
        import mp_start
        await mp_start.start_mp(get_case(1000))
    else:
        executor = ThreadPoolExecutor(max_workers=4)
        p = []
        for i in range(2961, 3041):
            p.append(loop.run_in_executor(executor, stub, i))
        for f in p:
            await f


def main():
    loop = asyncio.get_event_loop()
    discard_existed = True
    skip_dict = {
        "contour": 0,
        "smooth": 0,
        "basic": 0,
        "adv": 0,
        "closure": 0,
    }

    skip_list = [k for k, v in list(skip_dict.items()) if v]
    # If we do the skip, we cannot discard previous results
    if skip_list:
        discard_existed = False
    print(skip_list, discard_existed)
    utils.skip_list = skip_list
    loop.run_until_complete(run_radar(loop))


if __name__ == "__main__":
    main()
