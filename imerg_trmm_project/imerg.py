mode = "imerg"

central_meridian = -96.0
standard_parallel_1 = 20.0
standard_parallel_2 = 60.0
latitude_of_origin = 40.0

cnt_folder = "cnt"
cnt_polygon_folder = "cnt_polygon"
stage1_folder = "basic_metric"
stage2_folder = "adv_metric"

# Running config
base_folder = r'C:\Users\Administrator\Documents\IMERG'
contour_level = [3, 5, 7]
search_radius = 500e3
