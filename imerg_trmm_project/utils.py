# coding=utf-8

from __future__ import print_function

import glob
import tempfile
import threading
from builtins import zip
from builtins import range
import os
import sys
import random
import shutil
import datetime
import time
import numpy
import re

import arcpy


def create_dirs(dirs, discard=False):
    """helper function: create folder structure"""
    for p in [os.path.join(base_folder, d) for d in dirs]:
        if p.find("in_memory") != -1:
            continue
        if discard:
            print("Removing ", os.path.abspath(p))
            shutil.rmtree(p, ignore_errors=True)
        if not os.path.exists(p):
            os.makedirs(p)
    pass


def relocate(old_path, new_folder=None, new_ext=None, suffix=""):
    """relocate file at ```old_path``` to ```new_folder``` and change its extension to ```new_ext```"""
    if new_folder is None:
        new_folder = arcpy.env.workspace
    elif new_folder == ".":
        new_folder = temp_folder
    remove_all_dots = new_folder == "in_memory"
    base, ext = os.path.splitext(os.path.basename(old_path))
    base = base.replace(".", "_").replace(" ", "_").replace("-", "_")
    if new_ext is None:
        new_name = os.path.join(new_folder, base + suffix + ext)
    elif new_ext is "":
        new_name = os.path.join(new_folder, base + suffix)
    else:
        # Add a "." in case forgotten
        new_name = os.path.join(new_folder, base + suffix + (new_ext if new_ext.startswith(".") else ".%s" % new_ext))
    if remove_all_dots:
        new_name = new_name.replace(".", "_")
    print(f"Requested a new name={new_name}")
    return new_name


def list_folder_sorted_ext(folder=".", ext=None):
    return sorted(glob.glob(f"{folder}\\*{ext}"))


def smart_lookup_date(dstring, dformat, try_again=False):
    a = re.compile(r"[-_\s:\.]")
    # Let us trim string first.
    dstring = a.sub("", dstring)
    dformat = a.sub("", dformat)
    dformat_len = len(datetime.datetime.strftime(datetime.datetime.now(), dformat))
    # print("date string length is", dformat_len)
    for i in range(len(dstring)):
        # End of search
        if i + dformat_len > len(dstring):
            break
        sub_dstring = dstring[i:i + dformat_len]
        try:
            p_datetime = datetime.datetime.strptime(sub_dstring, dformat)
        except ValueError:
            continue
        assert (isinstance(p_datetime, datetime.datetime))
        if 1950 < p_datetime.year < 2050:  # 100 year range should be long enough
            print(("Found datetime", p_datetime))
            return p_datetime
    if try_again:
        return None
    # We didn't find a datetime at all!
    # Well let us try if we can use a shorter one without seconds
    else:
        return smart_lookup_date(dstring, dformat.replace("%S", ""), True)


def __find_files_in_list_by_time(files, ref_time, dformat, mask_config=None, allow_diff_sec=300):
    if not files:
        return None, None
    r = time.mktime(ref_time.timetuple())
    # We need to be smart enough to find file name
    t = [smart_lookup_date(os.path.basename(os.path.splitext(f)[0]), dformat) for f in files]
    s = numpy.array([time.mktime(p.timetuple()) for p in t])
    d = numpy.abs(s - r)
    if numpy.min(d) > allow_diff_sec:
        return None, None
    i = numpy.argmin(d)
    mask = None
    if mask_config is not None:
        for m in mask_config:
            if m[0] <= t[i] < m[1]:
                mask = m[2]
                break
    return mask, files[i]


def list_files_by_timestamp(basedir, timelist, dformat, file_ext=None, mask_config=None, allow_diff_sec=300):
    """find a list of files closest to given timelist"""
    files = list_folder_sorted_ext(basedir, file_ext)
    return list(zip(*[__find_files_in_list_by_time(files, t, dformat) for t in timelist]))


class TempArcpyEnv:
    """Temporarily switch to new workspace"""
    _lock = threading.RLock()

    def __init__(self, new_env="in_memory", delete=True):
        temp_suffix = f"_{os.getpid()}_{random.randint(1000, 9999)}"
        self.old_env = arcpy.env.workspace
        if new_env == "in_memory":
            self.new_env = new_env
        elif os.path.isabs(new_env):
            new_env += temp_suffix
            os.makedirs(new_env, exist_ok=True)
            self.new_env = new_env
        else:
            new_env = os.path.join(temp_folder, "scratch", new_env + temp_suffix)
            os.makedirs(new_env, exist_ok=True)
            self.new_env = new_env
        self.delete = delete

    def __enter__(self):
        self.__class__._lock.acquire()
        arcpy.env.workspace = self.new_env
        arcpy.env.overwriteOutput = True
        print(f"Workspace={arcpy.env.workspace}")

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.__class__._lock.release()
        arcpy.env.workspace = self.old_env
        arcpy.env.overwriteOutput = True
        if self.new_env == "in_memory" and self.delete:
            arcpy.Delete_management('in_memory')
        elif self.delete:
            shutil.rmtree(self.new_env, ignore_errors=True)
        print(f"Workspace={arcpy.env.workspace}")


def calc_field(feature, fields_dict, silent=False):
    __fields = [p.name for p in arcpy.ListFields(feature)]
    for k, v in list(fields_dict.items()):
        if isinstance(v, (list, tuple)):
            t = v[1]
            v = v[0]
        else:
            t = "DOUBLE"
        if k not in __fields:
            arcpy.AddField_management(feature, k, t)
        arcpy.CalculateField_management(feature, k, v, "PYTHON")
        if not silent:
            print("Calculate %s = %s in %s" % (k, v, feature))


def rename_field(feature, old_field, new_field):
    calc_field(feature, {new_field: '!%s!' % old_field})
    arcpy.DeleteField_management(feature, old_field)


# Default config
mode = "imerg"
central_meridian = -96.0
standard_parallel_1 = 20.0
standard_parallel_2 = 60.0
latitude_of_origin = 40.0
# Running config
base_folder = r'C:\Users\Administrator\Documents\IMERG'
contour_level = [3, 5, 7]
search_radius = 500e3
ibtrac = os.path.join(os.path.dirname(__file__), 'ibtracs.NA.list.v04r00.csv')
cnt_folder = "cnt"
cnt_polygon_folder = "cnt_polygon"
stage1_folder = "basic_metric"
stage2_folder = "adv_metric"


# Override default config
try:
    case = os.environ["CASENAME"].lower() \
        if "CASENAME" in os.environ else sys.argv[1].lower()
    config_path = os.path.join(os.path.dirname(__file__), case) + ".py"
    print("loading config from %s" % config_path)
    block = compile(open(config_path).read(), __name__, mode='exec')
    exec(block, globals(), locals())
except:
    import warnings
    warnings.warn("Failed to load a case, will use all defaults!")
    import traceback
    traceback.print_exc()
    traceback.print_stack()

os.makedirs(os.environ.get("E_TEMP", base_folder), exist_ok=True)
temp_folder = tempfile.mkdtemp(prefix=f"{mode}_", dir=os.environ.get("E_TEMP", base_folder))
os.environ["TEMP"] = temp_folder
