#coding=utf-8

"""This script calculate shape-metric those are NOT relavent to the coordinates of the eye"""
from __future__ import print_function

import os
import sys
import utils
from utils import calc_field, rename_field
import arcpy


def add_metric_to_polygon_with_equation(polygon, shape_metric, suffix=""):
    """Calculate basic shape metrics"""
    S = shape_metric
    s = {}
    for k, v in list(S.items()):
        field_name = k if k.find("%s") == -1 else k % suffix
        field_name = field_name[:10]
        s[field_name] = v
    calc_field(polygon, s)
    return list(s.keys())


def add_basic_geometry_attr(polygon, suffix=""):
    arcpy.AddGeometryAttributes_management(polygon, "AREA_GEODESIC;PERIMETER_LENGTH_GEODESIC",
                                           Length_Unit="KILOMETERS", Area_Unit="SQUARE_KILOMETERS")
    rename_field(polygon, "AREA_GEO", "AREA%s" % suffix)
    rename_field(polygon, "PERIM_GEO", "PERIM%s" % suffix)
    # rename_field(polygon, "CENTROID_X", "CNTRX%s" % suffix)
    # rename_field(polygon, "CENTROID_Y", "CNTRY%s" % suffix)
    return ["AREA%s" % suffix, "PERIM%s" % suffix] # "CNTRX%s" % suffix, "CNTRY%s" % suffix]


def execute(input_feat, output_feat):

        with utils.TempArcpyEnv():
            # Smooth polygons
            arcpy.CopyFeatures_management(input_feat, output_feat)
            arcpy.DeleteField_management(output_feat, ["MIN", "MAX", "MEAN", "RANGE", "STD", "SUM"])
            # Add shape metrics
            add_basic_geometry_attr(output_feat)
            # Create unique name in memory workspace
            box_shp = utils.relocate(output_feat, suffix="_box")
            cvx_shp = utils.relocate(output_feat, suffix="_ch")

            # # Then calculate elongation
            # arcpy.MinimumBoundingGeometry_management(output_feat,  box_shp, "RECTANGLE_BY_AREA", mbg_fields_option="MBG_FIELDS")
            # # This is an in-memory feature, so not limited to 10-letters filed name
            # arcpy.JoinField_management(output_feat, "FID",  box_shp, "ORIG_FID", fields=["MBG_Width", "MBG_Length", "MBG_Orient"])
            # calc_field(output_feat, {"Elongation": "!MBG_Width!/!MBG_Length!"})
            # # Then we need change name of the MBG_WIDTH and MBG_LENGTH to avoid future conflict
            # rename_field(output_feat, "MBG_Width", "BOX_WIDTH")
            # rename_field(output_feat, "MBG_Length", "BOX_LENGTH")
            # rename_field(output_feat, "MBG_Orient", "BOX_ORIENT")
        
            # Then calculate convex hull for the polygon
            arcpy.MinimumBoundingGeometry_management(output_feat, cvx_shp, "CONVEX_HULL", mbg_fields_option="NO_MBG_FIELDS")
            cvx_fields = add_basic_geometry_attr(cvx_shp, suffix="_CVX")
            arcpy.JoinField_management(output_feat, "FID", cvx_shp, "ORIG_FID", fields=cvx_fields)

            # Related to convex hull, moved from dispersiveness
            calc_field(output_feat, {"SOLIDITY": "!AREA!/!AREA_CVX!", "CONVEXITY": "!PERIM_CVX!/!PERIM!", "COMPACT": "(!AREA! ** 0.5)/(0.282 * !PERIM!)"})
            print("OK")


        
    
    


