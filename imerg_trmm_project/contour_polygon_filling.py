﻿from __future__ import print_function
from builtins import map
from builtins import range
__author__ = 'jtang8756'

import os
from unittest import TestCase

import utils
from add_basic_geometry import calc_field
import arcpy
import arcpy.da
import re


def distance_to_eye(lon, lat) -> float:
    eye_lon = float(os.environ["EYE_LON"])
    eye_lat = float(os.environ["EYE_LAT"])
    from pyproj import Geod
    g = Geod(ellps="WGS84")
    _, _, dist = g.inv(eye_lon, eye_lat, lon, lat)
    return dist


def retrieve_rain_level(feature_name):
    return float(re.search(r"_(\d)+mm", os.path.basename(feature_name)).group(1))


def execute(in_feature, out_feature, raster):

    arcpy.AddField_management(in_feature, "DIST_EYE", "DOUBLE", "", "", "20", "", "NULLABLE", "NON_REQUIRED", "")
    with arcpy.da.UpdateCursor(in_feature, ["SHAPE@XY", "DIST_EYE"]) as cursor:
        for row in cursor:
            lon, lat = row[0]
            row[1] = distance_to_eye(lon, lat)
            cursor.updateRow(row)
    with utils.TempArcpyEnv():
        out1 = utils.relocate(in_feature, suffix="_in")
        arcpy.Select_analysis(in_feature, out1, f"DIST_EYE < {utils.search_radius}")
        out2 = utils.relocate(in_feature, suffix="_g")
        arcpy.FeatureToPolygon_management(out1, out2)
        arcpy.AddField_management(out2, "F_AREA", "DOUBLE", "", "", "20", "", "NULLABLE", "NON_REQUIRED", "")
        arcpy.CalculateGeometryAttributes_management(out2, [["F_AREA", "AREA_GEODESIC"]],
                                                     area_unit="SQUARE_KILOMETERS")
        out3 = utils.relocate(in_feature, suffix="_area")
        arcpy.Select_analysis(out2, out3, f"F_AREA >= 3800")
        match_count = int(arcpy.GetCount_management(out3).getOutput(0))
        zone_dbf = utils.relocate(in_feature, suffix="_z", new_ext=".dbf")
        if match_count:
            calc_field(out3, {"ID_1": ["!FID!", "LONG"]})
            arcpy.sa.ZonalStatisticsAsTable(out3, "ID_1", raster, zone_dbf, "DATA", "ALL")
            arcpy.JoinField_management(out3, "ID_1", zone_dbf, "ID_1")
            out4 = utils.relocate(in_feature, suffix="_L")
            arcpy.CopyFeatures_management(out3, out4)
            rain_level = retrieve_rain_level(out4)
            arcpy.SelectLayerByAttribute_management(out4, "NEW_SELECTION", f"MEAN >= {rain_level}")
            if int(arcpy.GetCount_management(out4).getOutput(0)):
                arcpy.CopyFeatures_management(out4, out_feature)
                print("OK")
                return True
            else:
                print("EMPTY")
    return False


class TestPolygon(TestCase):

    def test_fill_polygon(self):
        os.environ["EYE_LAT"] = "30.7"
        os.environ["EYE_LON"] = "-74.0"
        execute('D:\\tmp\\cnt\\IMERG_20000913_S060000_E062959_1mm.shp',
                'D:\\tmp\\cnt_polygon\\IMERG_20000913_S060000_E062959_1mm.shp',
                'D:\\tmp\\IMERG.20000913-S060000-E062959.img')
